#!/bin/sh

set -ue

list_cross_compile_toolchains () { xbps-query --regex -Rs 'cross-(\w)+-(\w)+-(\w)+' ; }

bootstrap () {
	local variant=$1 && shift
	local cmd="./xbps-src -m masterdir-$variant binary-bootstrap x86_64";
	[ ! -z $variant -a "$variant" != 'glibc' ] && cmd="${cmd}-${variant}"

	${cmd}
}

# Name any alternative masterdir with a suffix of -<arch> in order for this to work
build_pkg () {
	[ $# -eq 3 ] || (echo 'Usage: ./x build_pkg <package> <arch> <variant>' && exit 1)
	local package=$1 && shift
	local arch=$1 && shift
	local variant=$1 && shift

	local cmd='./xbps-src -f -r /'
	#[ ! -z $arch ] && cmd="${cmd} -a ${arch}-${variant:-musl}"
	[ ! -z $arch -a "$arch" != 'x86_64' ] && cmd="${cmd} -a ${arch}" &&
		[ ! -z $variant -a "$variant" != 'glibc' ] && cmd="${cmd}-${variant}"
	[ ! -z $variant ] && cmd="${cmd} -m masterdir-${variant}"
	echo $cmd ...
	cmd="${cmd} pkg $package"

	echo "WILL RUN NOW: ${cmd}"
	${cmd}
}
build_variant () {
	local variant=$1 && shift
	local package=$1 && shift

	[ -d masterdir-$variant ] || bootstrap $variant

	for arch in x86_64 aarch64 ; do
	#for arch in x86_64 ; do
		echo "NEXT UP: build_pkg $package $arch $variant" && echo
		#echo "NEXT UP: build_pkg $package $arch $variant --- Press ENTER" && read keys
		build_pkg $package $arch $variant || exit 5
	done
}
build_glibc () { build_variant glibc $1 ; }
build_musl ()  { build_variant musl $1 ; }
build ()  {
        rm -f hostdir/binpkgs/${1:-NOTHING}*.xbps hostdir/binpkgs/${1:-NOTHING}*.xbps.sig* &&
                build_glibc $1 && build_musl $1
}


clean ()       { echo "Please run either clean_glibc or clean_musl." ; }
clean_glibc () { ./xbps-src -m masterdir-glibc clean ; }
clean_musl ()  { ./xbps-src -m masterdir-musl -a ${2:-x86_64}-musl clean ; }
zap_glibc ()   { ./xbps-src -m masterdir-glibc zap ; }
zap_musl ()    { ./xbps-src -m masterdir-musl -a ${2:-x86_64}-musl zap ; }


install () { sudo xbps-install --repository hostdir/binpkgs $@ ; }
remove () { xbps-remove -R $@ ; }

install_remote () {
	xbps-install --repository https://kevcrumb.gitlab.io/split-packages/musl $@
}


sign_pkg () {
	local readonly PKG_NAME=${1} && shift
	local readonly RSA_KEY=${1:-$HOME/.ssh/splitlinux/id_rsa}

	for p in hostdir/binpkgs/${PKG_NAME}-*.xbps ; do
		xbps-rindex --sign-pkg     --privkey ${RSA_KEY} \
			--signedby 'Kevin Crumb <kevcrumb@splitlinux.org>' ${p}
	done
}
sign_repo () {
	local readonly RSA_KEY=${1:-$HOME/.ssh/splitlinux/id_rsa}

	find hostdir/binpkgs/ -mindepth 1 -type d -exec rmdir {} +;

	for a in x86_64-musl aarch64-musl x86_64 ; do
		if [ -f hostdir/binpkgs/${a}-repodata ] ; then
			echo "$a ..."

			XBPS_TARGET_ARCH=$a xbps-rindex --clean hostdir/binpkgs/

			XBPS_TARGET_ARCH=$a xbps-rindex --sign     --privkey ${RSA_KEY} \
				--signedby 'Kevin Crumb <kevcrumb@splitlinux.org>' \
				hostdir/binpkgs/
			echo
		fi
	done
}
sign () { sign_pkg $1 && sign_repo ; }

deploy () { 
        if git ls-files --other --directory --exclude-standard | sed q1 ; then
                git rm -f --ignore-unmatch binpkgs/${1:-NOTHING}*.xbps binpkgs/${1:-NOTHING}*.xbps.sig*
                cp -a hostdir/binpkgs/${1:-NOTHING}*.xbps hostdir/binpkgs/${1:-NOTHING}*.xbps.sig2 hostdir/binpkgs/*-repodata binpkgs/
                git add binpkgs/${1:-NOTHING}*.xbps binpkgs/${1:-NOTHING}*.xbps.sig2 binpkgs/*-repodata
        else
                echo
                echo 'Untracked changed detected. If the files are unrelated, please remove them temporarily.' >&2
                echo 'If they are relevant, add them to git first. Then run "deploy" again.' >&2
        fi
}

eval "$*"

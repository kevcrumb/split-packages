#!/bin/sh

song="$*"

cd "`xdg-user-dir MUSIC`"

if [ -z "$song" ] ; then
        path=`find -mindepth 1 | sort | sed 's#^\.\/##g' |
                dmenu -i -l 10 -p "${PWD##*/}:" \
                        -nb "${BA_DMENU_COLOR1:-black}"  -nf "${BA_DMENU_COLOR2:-yellow}" \
                        -sb "${BA_DMENU_COLOR2:-yellow}" -sf "${BA_DMENU_COLOR1:-black}"`

        if [ -z "$path" ] ; then
                echo 'No track found or dmenu aborted.'
        else
                [ -f "${path}" ] && song=`basename "${path%.*}"`
                [ -d "${path}" ] && song="${path##*/}"
        fi
fi

if [ ! -z "$song" ] ; then
        mpc search filename "$song" | mpc insert && mpc next || mpc play
fi

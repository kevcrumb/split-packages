#!/bin/sh

readonly FILE="$1"
readonly FILENAME=`basename "${FILE}"`
readonly FILE_PATH=`realpath "${FILE}"`
readonly DIR=$(dirname "${FILE_PATH}")

# recursive
#readonly FILES=`find "${DIR}" -type f | sort`

# non-recursive
readonly FILES=`find "${DIR}" -maxdepth 1 -type f | sort`
readonly NUM=`echo "${FILES}" | grep --line-number --fixed-strings "${FILENAME}" | sed 's#\:.*$##g'`

echo "${FILES}" | sxiv -abrosf -n "${NUM}" -

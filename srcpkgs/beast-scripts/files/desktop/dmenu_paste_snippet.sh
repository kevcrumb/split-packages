#!/bin/bash


# For pasting to work with alacritty, configure it to paste from clipboard,
# not from selection. In $XDG_CONFIG_HOME/alacritty/alacritty.yml set:
#
# key_bindings:
#   - { key: Insert, mods: Shift, action: Paste }
#
# See https://github.com/alacritty/alacritty/blob/master/alacritty.yml


clip_and_paste () {
	local snippet=$1
	local keep=$2

 	# Backup CLIPBOARD to $backup
	local backup=`xsel -b`

	#beep -f 5000 -l 1000 &

        if [ -z "${snippet}" ] ; then
		snippet="`grep -R --no-filename --invert-match '^#' ${HOME}/.config/beast/snippets |
			sed '/^\s*$/d' | dmenu -b -l 20 \
                                -nb "${BD_DMENU_COLOR1:-black}"  -nf "${BD_DMENU_COLOR2:-yellow}" \
                                -sb "${BD_DMENU_COLOR2:-yellow}" -sf "${BD_DMENU_COLOR1:-black}" |
			                awk '{$1=\"\"; print $0}' | sed 's, *$,,g' | sed 's,^ *,,g'`"
	fi

        if [ "${snippet}" != '' ]; then
		# Copy snippet to CLIPBOARD
                # eval is used to support dynamic snippet parts like `date`
                eval echo -ne "${snippet}" | xsel -bi

        	xdotool key --clearmodifiers 'Shift_L+Insert'
        	#xdotool sleep 0.1 key --clearmodifiers 'Shift_L+Insert' sleep 0.1

        	#beep -f 3000 -l 30 &
        	if [ "$keep" == 'keep_in_clipboard' ]; then
        		echo "Keeping '"$1"' in clipboard"
        	else
		#	# Restore CLIPBOARD from $backup
        		echo "${backup}" | xsel -bi && sleep 0.5
        	fi
        fi
}

#show_buffers () {
#	echo "CLIPBOARD=`xsel -b`"
#	echo "PRIMARY=`xsel -p`"
#	echo "SECONDARY=`xsel -s`"
#}


#show_buffers
#echo '---'
#echo

clip_and_paste "$1" "$2"

#echo
#echo '---'
#show_buffers

#!/bin/sh


USER_DIRS_FILE="${XDG_CONFIG_HOME:-~/.config}/user-dirs.dirs"
[ -s "$USER_DIRS_FILE" ] && . "$USER_DIRS_FILE"


loop_executable () {
        readonly EXECUTABLE=${1:-}
        readonly ARGUMENTS=${2:-}

        echo "if type $EXECUTABLE ; then
                while true; do
				clear
				$EXECUTABLE $ARGUMENTS || sleep 2s
			done
		else
			$SHELL
	fi"
}


net_iw_pane () {
        tmux split-window -dv -- zsh -ic '
		if [ -z $WLAN_DEV ] ; then
			echo "\$WLAN_DEV not defined. Skipping iw command execution."
			$SHELL
		else
			if type iw ; then
		                while true; do
		                        clear
		                        local iw_cmd="iw dev ${WLAN_DEV} link | grep --color -E \"SSID.*|$\""
		
		                        echo "$iw_cmd\n──"
		                        eval $iw_cmd
		                        echo
		
		                        sleep 10s
		                done
			else
				$SHELL
			fi
		fi'
}

net_address_pane () {
        tmux split-window -dv -- zsh -ic '
                while true; do
                        clear
                        local ip_cmd="ip -brief -color -family inet address"

                        echo "$ip_cmd\n──"
                        eval $ip_cmd

                        sleep 10s
                done' 
}

net_route_pane () {
        tmux split-window -dh -- zsh -ic '
                while true; do
                        clear
                        local route_cmd="ip -color route"

                        echo "$route_cmd\n──"
                        eval $route_cmd

                        sleep 10s
                done'
}

net_window () {
	# TODO also tail vpn client logs
	net_route_pane
        net_address_pane
        net_iw_pane
}

music_window () {
	tmux new-window -n "$TWO" -- zsh -ic "`loop_executable vimpc`"
}

video_window () {
	tmux new-window -n "$THREE" -- zsh -ic "`loop_executable youtube-viewer '-4'`"
}


finance_balancesheet_pane () {
	tmux split-window -dvl 1  -- zsh -ic '
		if [ -f hledger.journal -a -f market-prices/in-usd.journal ] ; then
			hledger balancesheet --pretty --file hledger.journal --value=cost,XAU --file market-prices/in-usd.journal | less --chop-long-lines ;
		else
			echo "balancesheet\n(only shown when all required files are present)" | less ;
		fi'
}

finance_incomestatement_pane () {
        tmux split-window -d      -- zsh -ic '
		if [ -f hledger.journal -a -f market-prices/in-usd.journal ] ; then
			hledger incomestatement --pretty --file hledger.journal --quarterly --average --value=cost,XAU --file market-prices/in-usd.journal | less --chop-long-lines ;
		else
			echo "incomestatement\n(only shown when all required files are present)" | less ;
		fi'
}

finance_cashflow_pane () {
        tmux split-window -d      -- zsh -ic '
		if [ -f hledger.journal -a -f market-prices/in-usd.journal ] ; then
			hledger cashflow --pretty --file hledger.journal --value=cost,XAU --file market-prices/in-usd.journal | less --chop-long-lines ;
		else
			echo "cashflow\n(only shown when all required files are present)" | less ;
		fi'
        # - watch --differences --no-title -n 600 "hledger activity --daily --period='this week'"
}

finance_window () {
	if [ -z $XDG_LEDGER_DIR ] ; then
		tmux new-window -n "$FOUR" -- zsh -ic '
			echo "XDG_LEDGER_DIR not initialized. Set its path in ~/.config/user-dirs.dirs (shortcut command Vuserdirs)." ;
			zsh'
	else
		[ -d "$XDG_LEDGER_DIR" ] || mkdir -p "$XDG_LEDGER_DIR"

                cd "$XDG_LEDGER_DIR"

		# TODO hledger_show_stats_and_notes &&
		tmux new-window -n "$FOUR" -- zsh -ic "`loop_executable zsh`"
	
		if type hledger >/dev/null ; then
			finance_balancesheet_pane
			finance_incomestatement_pane
			finance_cashflow_pane
	
			tmux set-window-option -t "$FOUR"   main-pane-height  2
			tmux set-window-option -t "$FOUR"   other-pane-height 1
			tmux select-layout     -t "$FOUR"   main-horizontal
		fi
	fi
}


coding_window () {
	tmux new-window -n "$FIVE"   -- zsh -ic 'type units  && units  || $SHELL' &&
	        tmux split-window -d -- zsh -ic 'type python && python || $SHELL' &&
	        tmux split-window -d -- zsh -ic 'type pry    && pry    || $SHELL' &&
	        tmux split-window -d -- zsh -ic 'type ghci   && ghci   || $SHELL'

	tmux select-layout     -t "$FIVE"  main-vertical
}

# hook_to_attach_event () {
# 	position=$1 && shift
# 	name=$1     && shift
# 	cmd=$1      && shift
# 
# 	tmux set-hook "client-attached[$position]" "new-window -at $((position-1)) -n \"$name\"   -- zsh -ic \"$cmd\""
# }

rover_window () {
	tmux new-window -n "$SIX" -- zsh -ic "`loop_executable rover \
                '~ ~ "'$XDG_DOWNLOAD_DIR'" "'$XDG_PICTURES_DIR'" "'$XDG_VIDEOS_DIR'" "'$XDG_DOCUMENTS_DIR'" "'$XDG_MUSIC_DIR'" "'${TEMP:-/tmp}'"'`"
}


# TODO after clear: remind_dont_break_the_chain ; \
#                   find_birthdays_happening_within_three_days ; \
mainshell_window () {
        [ -d "${XDG_DOWNLOAD_DIR}" ] && cd "${XDG_DOWNLOAD_DIR}" || cd ~
	tmux new-window -n "$SEVEN" -- zsh -ic "tmux set-window-option -t 7 automatic-rename on && \
                                                `loop_executable zsh`"
}


todo_window () {
        tmux new-window -n "$EIGHT" -- zsh -ic '
                while true; do
                        ${EDITOR:-vi} "+/aa)\." -c "nohl" "$HOME/todo.textile"
                done' &&
                tmux split-window -dh -- zsh -ic '
			while true; do
                                clear && echo "\033[0;32m" && cal -v `date --date="$(date +%Y-%m-01) - 1 month" "+%m %Y"` &&
                                echo && cal -vw && echo && cal -v `date --date="$(date +%Y-%m-01) + 1 month" "+%m %Y"`
                                sleep 30m
                        done'
	
	# When the X window containing the tmux session is shrunk and then resized back
	# to full size, the "other-pane" stays wider than it should be.
	# See http://superuser.com/questions/1160725/how-can-i-have-tmux-restore-pane-widths-after-resizing-the-terminal
	# for news regarding the issue.
	tmux set focus-events on
	for event in client-attached pane-focus-in ; do
	        tmux set-hook -t "${SESSION}" "${event}[8]"  "set-window-option -t '$EIGHT' main-pane-width  22"
	        tmux set-hook -t "${SESSION}" "${event}[81]" "set-window-option -t '$EIGHT' other-pane-width 21"
		tmux set-hook -t "${SESSION}" "${event}[82]" "select-layout     -t '$EIGHT' main-vertical"
	done
}


mail_window () {
	tmux new-window -n "$NINE" -- zsh -ic "`loop_executable mutt '-e \"push i\"'`"
}


configure_statusbar () {
	AVAILABLE_SPACE=20
	tmux set status-left-length   ${AVAILABLE_SPACE}
	tmux set status-left          '◤#{session_name}                  '

	tmux set status-justify       centre

	tmux set status-right-length  ${AVAILABLE_SPACE}
	tmux set status-right         '           %H:%M %Z'
	#tmux set status-right         "◣       #(echo TODO tmux_timezones.sh ${AVAILABLE_SPACE}) ◥"
}


launch_gecko () {
	transset -a 0.90 2>/dev/null

	readonly SESSION='🦎'

	# ◤ 🦎◢  〔🦎 〕 ⦗ 🦎 ⦘  「🦎 」 『🦎 』 〘🦎 〙 ｢🦎｣ 〈🦎 〉 ◣ 💣 ◥
	# 🧪 distorts the tmux status bar
	readonly ONE='〔📡 〕'
	readonly TWO='〔🎶 〕'
	readonly THREE='〔📺 〕'
	readonly FOUR='〔🗠  〕'
	readonly FIVE='〔🔮 〕'
	readonly SIX='〔🐚 〕'
	readonly SEVEN='〔💣 〕'
	readonly EIGHT='〔📝 〕'
	readonly NINE='〔📥 〕'

	tmux new-session -ds "${SESSION}" -c ~/ -n "$ONE" -- zsh -ic '
		tmux set-window-option -g automatic-rename off && echo Loading .. && sleep 3'
	tmux set-option   -t "${SESSION}" allow-rename     off

	echo 'net_window ...'          && net_window
	echo 'music_window ...'        && music_window
	echo 'video_window ...'        && video_window
	echo 'finance_window ...'      && finance_window
	echo 'coding_window ...'       && coding_window
	echo 'rover_window ...'        && rover_window
	echo 'mainshell_window ...'    && mainshell_window
	echo 'todo_window ...'         && todo_window
	echo 'mail_window ...'         && mail_window

	echo 'configure_statusbar ...' && configure_statusbar

        tmux select-window -t 7
	tmux -u2 attach -t "${SESSION}"
}


if [ -z $1 ] ; then
	if [ -e "${XDG_CONFIG_HOME:-~/.config}/beast/consoles/default.sh" ] ; then
		. "${XDG_CONFIG_HOME:-~/.config}/beast/consoles/default.sh"
	else
		launch_gecko
	fi
else
	if [ -e "${XDG_CONFIG_HOME:-~/.config}/beast/consoles/$1.sh" ] ; then
		. "${XDG_CONFIG_HOME:-~/.config}/beast/consoles/$1.sh"
	else
		echo "No script named '$1.sh' found in ${XDG_CONFIG_HOME:-~/.config}/beast/consoles/"
	fi
fi

#!/bin/bash

readonly IMG_DIR="${1:-$HOME/.config/beast/backgrounds}"
# TODO make $2 an integer representing the requested maxdepth
readonly RECURSIVE="${2}"

SCRIPTNAME=`basename $0`
STATUSFILE="${TMPDIR:-/tmp}/$SCRIPTNAME.$USER"
CURRENT_IMAGE=/dev/null
[ -s "${STATUSFILE}" ] && CURRENT_IMAGE="`<${STATUSFILE}`"

if [ -e "${IMG_DIR}" ]; then
        maxdepth=1
        [ -n "$RECURSIVE" ] && maxdepth=99

        VALID_NAMES=".*\.\(jpg\|gif\|png\|jpeg\)"

	image="${CURRENT_IMAGE}"

	images=`find "${IMG_DIR}/" -maxdepth ${maxdepth} -type f,l -regex "${VALID_NAMES}" |
		grep --invert-match --fixed-strings "${CURRENT_IMAGE}"`

	image_count=`echo "${images}" | wc -l`

	if [ ${image_count} -ge 2 -o "${images}" != "${image}" ] ; then
	        image=`echo "${images}" |
		       sort --random-sort --random-source=/dev/urandom | head -n1`
	fi

	echo "Replacing $CURRENT_IMAGE with ${image}." &&
                feh --bg-fill "${image}" &&
		BDupdate_statusbar " Set wallpaper: ${image}"

        realpath "${image}" > "${STATUSFILE}"
fi

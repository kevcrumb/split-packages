#!/bin/sh

TRASH_DIR=`mktemp -d --tmpdir .Trash-XXX`

available=`df --output=avail "${TMPDIR:-/tmp}" | tail -n1`
size=`du --total "$@" | tail -n 1 | awk '{ print $1 }'`

if [ $(($size * 2)) -lt $available ] ; then
        mv "$@" "$TRASH_DIR/"
else
        echo "ERROR: The file(s) would occupy more than half the available space in trash. Consider deleting instead." >&2
fi

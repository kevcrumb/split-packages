#!/bin/sh

content=''

if [ $# -ge 1 ] ; then
	content="$@"
else
	current_todo_item=`grep -A10 'aa)' ~/todo.textile |
		grep --max-count=1 '#' | sed 's|^#||g' | cut -c-60 | tr -d '\n'`

	default_route=`ip route show 0.0.0.0/1`
	[ -z "${default_route}" ] && default_route=`ip route show default`
	default_route_device=`echo "${default_route}" |
		sed --silent 's#.* dev \(.*\)#\1#p' | awk '{ print $1 }'`

	wireless_percentage=`awk 'NR==3 {print $3}' /proc/net/wireless | sed 's#\.$##g'`
	wireless_signal_strength=`echo -n ⚞"${wireless_percentage}"⚟`

	battery_charge_level="`grep --quiet Discharging /sys/class/power_supply/BAT*/status &&
		echo -n '⛫' || echo -n '⚡' ; BDget_battery_charge`"

	content=" ${current_todo_item}   ${default_route_device} ${wireless_signal_strength} ${battery_charge_level}"
fi

xsetroot -name "${content}"

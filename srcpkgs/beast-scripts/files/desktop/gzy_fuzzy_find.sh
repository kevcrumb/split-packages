#!/bin/sh

readonly SOURCE="$1" && shift
readonly QUERY="$@"

return_code=0

# fzf allows multi-select and direct opening if just one match is found
#FINDER="fzf --delimiter='/' --with-nth=4.. --nth=2.. --exact --tiebreak=end,length,index --select-1 --exit-0 --multi --query='"${QUERY}"'"
#MATCHES=`cat /tmp/.finddb | eval $FINDER`
#IFS=$'\n' && open ${MATCHES}

# fzy is faster than fzf
readonly FINDER='fzy'

# Using grep first allows for matching of whole words:
readonly GREP_MATCHER=`echo "${QUERY}" | sed 's#\.#\\\.#g' | sed 's#\s\+#.*#g'`


# In basic regular expressions the meta-characters ?, +, {, |, (, and ) lose their special meaning.
# Only if an upper-case character is found, search will be case-sensitive (aka "smart-case").
if echo "$GREP_MATCHER" | grep --quiet '[[:upper:]]' ; then
	GREP_MATCH=`grep --no-ignore-case --basic-regexp "${GREP_MATCHER}" "${SOURCE}"`
else
	GREP_MATCH=`grep --ignore-case    --basic-regexp "${GREP_MATCHER}" "${SOURCE}"`
fi


match_count=`echo "${GREP_MATCH}" | wc -l`
[ "$GREP_MATCH" = '' ] && match_count=0

if [ `expr ${match_count}` -gt 0 ]; then
        if [ `expr ${match_count}` -eq 1 ]; then
                echo "$GREP_MATCH"
        else
                echo "$GREP_MATCH" | $FINDER
		return_code=$?
        fi
else
        return_code=1
fi

exit ${return_code}

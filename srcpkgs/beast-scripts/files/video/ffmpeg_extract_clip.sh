#!/bin/sh

# You can call this script via a hotkey in mpv to extract a clip from the loop
# points you set within the player. Example definition of key "c" in mpv's
# input.conf:
# c run <THIS_SCRIPT> ${=ab-loop-a} ${=ab-loop-b} "${path}"

readonly FROM="${1}" && shift
readonly TO="${1}" && shift
readonly IN="${*}"

readonly in=`basename "${IN}"`
readonly name="${in%.*}"
extension="${in##*.}"

# As of 2023-05 extracting from avi to avi results in clip of reduced image qualilty.
[ "$extension" = 'avi' ] && extension=mp4

readonly duration="$(( ${TO%\.*} - ${FROM%\.*} ))"

readonly out_name="${name}-${FROM%\.*}_${duration%\.*}s"
out="${out_name}.${extension}"

[ -e "${out}" ] &&
        out=`mktemp "${out_name}_XXX.${extension}"` ||
        touch "${out}"

echo "Extracting selection to ${out} ..."

ffmpeg -loglevel error -i "${IN}" -ss "${FROM}" -to "${TO}" -y "${out}" &&
        file "${out}"

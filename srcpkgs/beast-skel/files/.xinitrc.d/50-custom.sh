# Adapt this file according to your needs.

# This determines which host Ctrl+Super+Alt combinations speak to (see dwm's
# config.h).
#export MPD_REMOTE_HOST=localhost

# By default fzopen indexes ${HOME}/*. Use this environment variable to
# specify the locations you need instead.
#export FZOPEN_PLACES="${HOME}/Pictures ${HOME}/Videos"

# Visually-consistent scaling is best achieved by optimizing the value of
# "Xft.dpi" in ~/.Xresources. The following environment variables may be used to
# further fine-tune Qt- and/or GTK-applictions.
#export GDK_SCALE=1
#export GDK_DPI_SCALE=1.3
#export QT_SCALE_FACTOR=1.3

# Set time zone. Command `find /usr/share/zoneinfo/ -printf '%P\n'` to list
# available zones.
# export TZ='UTC'

#{ killall redshift 2>/dev/null ;
#	abduco -fn 'redshift_override' redshift -l '42.595833:-72.227222' -m vidmode } &

setxkbmap -layout de -variant neo

${BEAST_TERMINAL:-alacritty} -e zsh -ic '
	while true; do
		tmux -2 attach &>/dev/null || BDlaunch_console
	done' &

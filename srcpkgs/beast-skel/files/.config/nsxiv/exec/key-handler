#!/bin/sh

# Called by sxiv(1) after the external prefix key (C-x by default) is pressed.
# The next key combo is passed as its first argument. Passed via stdin are the
# images to act upon, one path per line: all marked images, if in thumbnail
# mode and at least one image has been marked, otherwise the current image.
# sxiv(1) blocks until this script terminates. It then checks which images
# have been modified and reloads them.

# The key combo argument has the following form: "[C-][M-][S-]KEY",
# where C/M/S indicate Ctrl/Meta(Alt)/Shift modifier states and KEY is the X
# keysym as listed in /usr/include/X11/keysymdef.h without the "XK_" prefix.

readonly KEY="$1";
readonly TAGFILE="$HOME/.config/sxiv/tags"
readonly TMPFILE="/tmp/sxiv.$$"

rotate() {
	degree="$1"
	while read file; do
		case "$(file -b -i "$file")" in
		image/jpeg*) jpegtran -rotate "$degree" -copy all -outfile "$file" "$file" ;;
		*)           mogrify  -rotate "$degree" "$file" ;;
		esac
	done
}

tag_add() {
	>>"$TAGFILE"
	tags=$(dmenu <"$TAGFILE" | tr '\n' ',')
	[ -z "$tags" ] && return
	iptckwed -i -a "$tags"
	echo -n "$tags" | tr ',' '\n' | sort - "$TAGFILE" | uniq >"$TAGFILE.new"
	mv -f "$TAGFILE"{.new,}
}

tag_del() {
	cat >"$TMPFILE"
	tags=$(iptckwed -iql <"$TMPFILE" | cut -f 2 | tr ',' '\n' | sort | uniq | dmenu | tr '\n' ',')
	[ -z "$tags" ] && return
	iptckwed -i -r "$tags" <"$TMPFILE"
	rm -f "$TMPFILE"
}

case "$KEY" in
	# NOTE This configuration reserves all character keys for moving files to ~/Downloads/_sxiv/<CHARACTER>.
	#      See the *) condition.

        #"C-c")      tr '\n' ' ' | xsel -i ;;
        #"C-e")      while read file; do urxvt -bg "#444" -fg "#eee" -sl 0 -title "$file" -e sh -c "exiv2 pr -q -pa '$file' | less" & done ;;
        #"C-g")      tr '\n' '\0' | xargs -0 gimp & ;;
        #"less"|"Prior")  rotate 270 ;;
        "less")      rotate 270 ;;
        #"C-period") rotate  90 ;;
        "greater")   rotate  90 ;;
        #"C-slash")  rotate 180 ;;
        #"C-t")      tag_add ;;
        #"M-T")      tag_del ;;

	# Open marked images in gimp
        # NOTE Use '&' to avoid having sxiv wait for the command to complete.
        #      Use ';' to have sxiv wait for the command to finish and then reload the image.
        "Return")       while read file; do gimp "$file" & done ;;

	# Email marked images
        # TODO Add all files as attachment opposed to sending separate emails.
        # NOTE Set "me" as an alias referring to an email address in mutt.
        "Next")         while read file; do echo "$file" | mutt -s "`basename "$file"`" -a "$file" -- me & done ;;
        # TODO Add a key for using imagemagick_limit_dimensions_and_send.sh

	# Set marked image as wallpaper
        "BackSpace")    while read file; do feh --bg-fill "$file" & done ;;

        # TODO
        #"t")           while read file; do realpath=`realpath "$file"` ; tmux_neww_selected_directory.sh "`dirname "${realpath}"`" & done ;;

	# Move marked images to /tmp so that the next reboot deletes it
        "Delete")
                # TODO Consider initializing a trash + corresponding env var upon startup (~/.config/user-dirs.dirs).
                while read file; do
                        TRASH=`mktemp --directory --tmpdir sxiv_trash-XXX`
                        mv "$file" "$TRASH/"
                done
                ;;

        # Create a copies of the marked images
        "Insert")       while read file; do mkdir -p "${HOME}/Downloads/_sxiv/cp/" && cp -v "${file}" "${HOME}/Downloads/_sxiv/cp/" ; done ;;

	# Let user select directory to move marked images to
        "Tab")
		while read file; do
			if [ -z $DESTINATION_DIR ] ; then
				PICTURES_DIR=`xdg-user-dir PICTURES || $HOME` && cd "$PICTURES_DIR/"
				DESTINATION_DIR=`find -mindepth 1 -type d | dmenu -l 20` && cd -
			fi

			mv -v "${file}" "${PICTURES_DIR}/${DESTINATION_DIR}/"
		done
		unset DESTINATION_DIR
		;;

        # Consider any character key as a directory name within ~/Downloads/_sxiv/.
        # TODO Use user-dirs.dirs to determine "Downloads" directory.
        *)           while read file; do mkdir -p "${HOME}/Downloads/_sxiv/${KEY}/"  && mv -v "${file}" "${HOME}/Downloads/_sxiv/${KEY}/" ; done ;;
esac

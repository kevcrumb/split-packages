" This is Beast's default NVIM configuration.
" Instead of a manager like vim-plug or vundle, NeoVIM's builtin plugin manager is used.
" To install a plugin just clone it to the corresponding destination.
"
" Examples:
"   git clone https://github.com/kien/ctrlp.vim ~/.config/nvim/pack/common/start/ctrlp


set nocompatible              " be iMproved, required
filetype off                  " required


" Use whole "words" when opening URLs.
" This avoids cutting off parameters (after '?') and anchors (after '#').
" See http://vi.stackexchange.com/q/2801/1631
let g:netrw_gx="<cWORD>"
"let g:netrw_browsex_viewer="open"


" CTAG Support: Vim will automatically read the 'tags' file, but we'll have to
"               tell it to read the 'gems.tags' file too.
set tags+=gems.tags

" Enable file type detection.
" Use the default filetype settings, so that mail gets 'tw' set to 72,
" 'cindent' is on in C files, etc.
" Also load indent files, to automatically do language-dependent indenting.
filetype plugin indent on    " required

let mapleader = "\<Space>"

if has("autocmd")
        " Set hight of location list:
        "autocmd VimEnter * lopen 3

        " Remove ALL autocommands for the current group.
        ":autocmd!

        "autocmd BufRead,BufNewFile *.textile setlocal spell
        " word completion (add words to dictionary using command "zg")
        set complete+=kspell
endif


" ["x]Y                  yank [count] lines [into register x] (synonym for
"                        yy, linewise).  If you like "Y" to work from the
"                        cursor to the end of line (which is more logical,
"                        but not Vi-compatible) use ":map Y y$".
map Y y$

if has('nvim')
        set mouse=

        " NVIM clipboard-support (use * for primary or + for clipboard)
        noremap <Leader>d "+d
        noremap <Leader>D "+D
        noremap <Leader>p "+p
        noremap <Leader>P "+P
        noremap <Leader>y "+y
        noremap <Leader>Y "+y$
endif

" Search entire words
nmap ? /\<\><Left><Left>

nnoremap <Leader>. ''<CR>
" Jump to "active"-list (useful in todo.textile)
nnoremap <Leader>a /(active aa<CR>:nohl<CR>0<Down><Down>
nnoremap <Leader>b :w<CR>:b#<CR>
nnoremap <Leader>B :CtrlPBuffer<CR>
" nnoremap <Leader>d :CtrlPBufTag<CR>
" Extract variable: Use after cw<Esc> to create a new variable of the changed value
" (The additional <Esc> at the beginning is to avoid delays on the O-command.) 
nnoremap <Leader>e <Esc>O<C-r>.=<C-r>"<Esc>
" Jump to "once"-list (useful in todo.textile)
nnoremap <Leader>i /(once ii<CR>:nohl<CR>0<Down><Down>
nnoremap <Leader>l :ll<CR>
nnoremap <Leader>L :lclose<CR>
nnoremap <Leader>n :n<CR>
nnoremap <Leader>o :CtrlP<CR>
nnoremap <Leader>r :set invwrap<CR>
nnoremap <Leader>s- :set nospell nolist<CR>
" Spell-check the current document in certain language
nnoremap <Leader>sd :set spell spelllang=de<CR>
nnoremap <Leader>se :set spell spelllang=en<CR>
nnoremap <Leader>ss :set spell spelllang=es<CR>
nnoremap <Leader>si :set spell spelllang=id<CR>
nnoremap <Leader>sp :set spell spelllang=pt<CR>
nnoremap <Leader>sr :set spell spelllang=ru<CR>
" Outline/display otherwise invisible characters like tabs, line breaks,
" escape sequences
nnoremap <Leader>sx :set list listchars=eol:¬,tab:>·,trail:~,extends:>,precedes:<,space:␣<CR>
nnoremap <Leader>S :SyntasticInfo<CR>
nnoremap <Leader>t :TagbarOpenAutoClose<CR>
nnoremap <Leader>T :TagbarToggle<CR>
" Uppercase the first letter of every word
nnoremap <Leader>U :%s/\<./\u&/cg<CR>
" Paste result of yanked formula
" With VIM's internal calculator:
"nnoremap <Leader>v <Esc>i<C-r>=<C-r>"<Esc><Esc>
" With bc (more accurate):
"nnoremap <Leader>v :let bc_result=systemlist("echo '<C-r>"' \| bc")<Esc>i<C-r>=bc_result[0]<Esc><Esc>
" With bc (using implicit scale):
"nnoremap <Leader>v :<Esc>i<C-r>=systemlist("echo '<C-r>"' \| bc")[0]<Esc><Esc>
" With bc (using explicit scale):
nnoremap <Leader>v :<Esc>i<C-r>=systemlist("echo 'scale=12; <C-r>"' \| bc")[0]<Esc><Esc>
nnoremap <Leader>w :w<CR>
nnoremap <Leader>q :q<CR>
nnoremap <Leader>x :x<CR>

" Insert current file name without its extension
nnoremap <Leader>f i<C-R>=expand("%:t:r")<CR><Esc>

" Disable arrow keys (for the purists)
"inoremap <Left>  <NOP>
"inoremap <Right> <NOP>
"inoremap <Up>    <NOP>
"inoremap <Down>  <NOP>
"nnoremap <Left>  <NOP>
"nnoremap <Right> <NOP>
"nnoremap <Up>    <NOP>
"nnoremap <Down>  <NOP>


" Navigate split windows
nnoremap <Leader>j <C-W><C-J>
nnoremap <Leader>k <C-W><C-K>
"nnoremap <Leader>l <C-W><C-L>
nnoremap <Leader>h <C-W><C-H>
" nnoremap <C-J> <C-W><C-J>
" nnoremap <C-K> <C-W><C-K>
" nnoremap <C-L> <C-W><C-L>
" nnoremap <C-H> <C-W><C-H>


syntax enable

" Improve search behaviour
set hlsearch
set incsearch
" make searches case-insensitive except when it includes upper-case characters
set ignorecase smartcase
" set wildignore+=*/tmp/*,*.so,*.swp,*.zip


" Use spaces instead of tabs
set expandtab
set modeline
set background=dark
" colorscheme koehler
hi clear SpellBad
hi SpellBad cterm=underline ctermfg=red

set cursorline
highlight CursorLine   cterm=NONE ctermbg=235
set cursorcolumn
highlight CursorColumn cterm=NONE ctermbg=235
"nnoremap <Leader>c :set cursorline! cursorcolumn!<CR>
" Reset highlightings and show current file path
nnoremap <Leader><Leader> :nohl<CR>:set nospell nolist<CR>1<C-G>


" Color cursor depending on current mode
" Worked in 04/15 in urxvt *without* tmux
"if &term =~ "xterm\\|rxvt"
"        " use a blue cursor in insert mode
"        let &t_SI = "\<Esc>]12;blue\x7"
"        " use a red cursor otherwise
"        let &t_EI = "\<Esc>]12;red\x7"
"        silent !echo -ne "\033]12;red\007"
"        " reset cursor when vim exits
"        autocmd VimLeave * silent !echo -ne "\033]112\007"
"        " use \003]12;gray\007 for gnome-terminal
"endif


au BufRead,BufNewFile *.journal set filetype=ledger
au BufRead,BufNewFile *.thor set filetype=ruby
au BufRead,BufNewFile *.vcf set filetype=vcard


" -------------------------------------
" Search and replace visual selection
" (with support for special characters)

" Escape special characters in a string for exact matching.
" This is useful to copying strings from the file to the search tool
" Based on this -
" http://peterodding.com/code/vim/profile/autoload/xolox/escape.vim
function! EscapeString (string)
        let string=a:string
        " Escape regex characters
        let string = escape(string, '^$.*\/~[]')
        " Escape the line endings
        let string = substitute(string, '\n', '\\n', 'g')
        return string
endfunction

" Get the current visual block for search and replaces
" This function passed the visual block through a string escape function
" Based on this -
" http://stackoverflow.com/questions/676600/vim-replace-selected-text/677918#677918
function! GetVisual() range
        " Save the current register and clipboard
        let reg_save = getreg('"')
        let regtype_save = getregtype('"')
        let cb_save = &clipboard
        set clipboard&

        " Put the current visual selection in the " register
        normal! ""gvy
        let selection = getreg('"')

        " Put the saved registers and clipboards back
        call setreg('"', reg_save, regtype_save)
        let &clipboard = cb_save

        "Escape any special characters in the selection
        let escaped_selection = EscapeString(selection)

        return escaped_selection
endfunction

" Start the find and replace command across the entire file
"vmap <Leader>z <Esc>:%s/<c-r>=GetVisual()<cr>/
vmap <Leader>z <Esc>:%s/<c-r>=GetVisual()<cr>//cg<left><left><left>
" -------------------------------------


" Softwrap at specific column
" nnoremap <Leader>b :set wrap linebreak nolist columns=75<CR>
nnoremap <Leader>c :set   linebreak columns=72<CR>
" nnoremap <Leader>C :set   linebreak columns=96<CR>
nnoremap <Leader>C :set   linebreak columns=129<CR>
" set linebreak columns=72


" """ PLUGIN-SPECIFIC OPTIONS
" Since Plugins are loaded AFTER vimrc is processed, exists() checks will only
" work later on.
" http://superuser.com/a/931316
"
" These options are supposed to be set in the after/plugin directory, but this
" way allows us to manage all settings in a single configuration file.
function! SetPluginOptionsNow()
        if exists(':CtrlP')
                " let g:ctrlp_custom_ignore = '\v[\/]\.(git|hg|svn)$'
                let g:ctrlp_custom_ignore = {
                                        \ 'dir':  '\v[\/]_site$',
                                        \ 'file': '\v\.(exe|so|dll)$',
                                        \ 'link': 'some_bad_symbolic_links',
                                        \ }
        "else
        "        echo "CtrlP is missing"
        endif

        if exists(':SyntasticInfo')
                set statusline+=%#warningmsg#
                set statusline+=%{SyntasticStatuslineFlag()}
                set statusline+=%*

                let g:syntastic_always_populate_loc_list = 1
                let g:syntastic_auto_loc_list = 1
                let g:syntastic_check_on_open = 1
                let g:syntastic_check_on_wq = 0
                "let g:syntastic_aggregate_errors = 1
                let g:syntastic_html_tidy_exec = 'tidy5'
                let g:syntastic_loc_list_height = 2
        "else
        "        echo "Syntastic is missing"
        endif

        if exists(':VroomRunTestFile')
                let g:vroom_test_unit_command='testrbl -Itest'
                "let g:vroom_use_spring=1
                let g:vroom_use_vimux=1
                let g:vroom_use_zeus=0
        "else
        "        echo "Vroom is missing"
        endif
endfunction
autocmd VimEnter * call SetPluginOptionsNow()
""" END OF PLUGIN-SPECIFIC OPTIONS

# Load shell plugin manager
# 
# zgen loads 100 ms faster than zplug.
# antibody loads 30 ms faster than zgen.
# https://getantibody.github.io/


script=.zsh_plugins.sh 
plugins_file="$HOME/$script"

if [ ! -f "$plugins_file" -o ! -d $HOME/.cache/antibody ] ; then
	if [ -f "$plugins_file" ] ; then
		backup=`mktemp --tmpdir $script-backup-XXX`
		echo Cache directory missing, but $script is present.
		echo Moving it to $backup ...
		echo
		mv "$plugins_file" $backup
	fi

	echo Bundling antibody plugins ...
	echo Using a temporary file as protection against interruptions.

	echo '# This file is reserved' > "$plugins_file"
	tmp=`mktemp --tmpdir $script-XXX`

	sh -c "torsocks -a ${TOR:-172.18.0.2} -P 9050 --isolate antibody bundle << EOF
ohmyzsh/ohmyzsh

# plugins
ohmyzsh/ohmyzsh path:plugins/command-not-found

# inserts 'sudo' or 'sudoedit' before the command when hitting [Esc][Esc]
#ohmyzsh/ohmyzsh path:plugins/sudo
#ohmyzsh/ohmyzsh path:plugins/vi-mode
#ohmyzsh/ohmyzsh path:plugins/web-search
# NOTE In 'st', syntax-highlighting causes undesired double characters to be
# output upon typing
zsh-users/zsh-syntax-highlighting
# Directory listings for zsh with git features
rimraf/k

# completions
zsh-users/zsh-completions

# theme
ohmyzsh/ohmyzsh path:themes/arrow.zsh-theme
EOF"	> $tmp

	sed "s#$HOME#~#g" $tmp > "$plugins_file" &&
		echo Done.
fi

# $ZSH tells oh-my-zsh.sh where to find subdirectories.
ZSH=`sed --silent "s|^source ~\(.*\)/oh-my-zsh.sh$|${HOME}\1|p" "$plugins_file"` \
	source "$plugins_file"
